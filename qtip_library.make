core = 7.x
api = 2

libraries[qtip][download][type] = file
libraries[qtip][download][filename] = qtip2.tar.gz
libraries[qtip][download][url] = https://github.com/Craga89/qTip2/tarball/master
